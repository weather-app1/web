import AuthApi from "~/services/weather/AuthApi";
import CityApi from "~/services/weather/CityApi";
import CurrentLocationApi from "~/services/weather/CurrentLocationApi";
import ForecastApi from "~/services/weather/ForecastApi";
import UserLikesApi from "~/services/weather/UserLikes";
import WeatherApi from "~/services/weather/WeatherApi";
export default interface CitySunApi {
  auth: AuthApi;
  city: CityApi;
  currentLocation: CurrentLocationApi;
  forecast: ForecastApi;
  userLikes: UserLikesApi;
  weather: WeatherApi;
}
