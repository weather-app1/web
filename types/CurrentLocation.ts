export default interface CurrentLocation {
  city: String;
  country: String;
  iso2: String;
  latitude: String;
  longitude: String;
}
