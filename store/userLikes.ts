import { ActionTree, GetterTree, MutationTree } from "vuex";

import { RootState } from "~/store";

export const state = () => ({
  likedCities: [] as Array<any>
});

export type UserLikesState = ReturnType<typeof state>;

export const getters: GetterTree<UserLikesState, RootState> = {
  //
};

export const mutations: MutationTree<UserLikesState> = {
  setLikedCities: (state: UserLikesState, likedCities) => {
    state.likedCities = likedCities;
  }
};

export const actions: ActionTree<UserLikesState, RootState> = {
  //
};
