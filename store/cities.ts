import { ActionTree, GetterTree, MutationTree } from "vuex";
import Pagination from "~/types/pagination";

import { RootState } from "~/store";
import helperPlugin from "~/plugins/helper.plugin";

export const state = () => ({
  cities: [] as Array<Object>,
  pagination: {} as Pagination,
  loading: false as boolean
});

export type CitiesState = ReturnType<typeof state>;

export const getters: GetterTree<CitiesState, RootState> = {
  //
};

export const mutations: MutationTree<CitiesState> = {
  setCities: async (state: CitiesState, cities: Array<Object>) => {
    state.cities = cities;
  },
  addCity: (state: CitiesState, city: Object) => {
    state.cities.unshift(city);
  },
  setPagination: (state: CitiesState, pagination: Pagination) => {
    state.pagination = pagination;
  },
  setCityLoading: (state: CitiesState, loading: boolean) => {
    state.loading = loading;
  }
};

export const actions: ActionTree<CitiesState, RootState> = {};
