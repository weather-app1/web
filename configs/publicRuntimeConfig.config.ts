import { NuxtConfig } from "@nuxt/types";

export const publicRuntimeConfig: NuxtConfig["publicRuntimeConfig"] = {
  auth0: {
    domain: process.env.AUTH0_DOMAIN,
    client_id: process.env.AUTH0_CLIENT_ID
  }
};
