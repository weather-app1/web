import BaseApi from "../services/weather/BaseApi";
import routes from "../services/weather/route.config";

const baseUrl = `api/${BaseApi.VERSION}`

export const auth = {
  strategies: {
    local: {
      token: {
        property: "access_token"
      },
      user: {
        property: "data"
      },
      endpoints: {
        login: {
          url: `${baseUrl}/${routes.auth.login}`,
          method: "post"
        },
        logout: {
          url: `${baseUrl}/${routes.auth.logout}`,
          method: "post"
        },
        user: {
          url: `${baseUrl}/${routes.auth.me}`,
          method: "get"
        }
      }
    },
    auth0: {
      domain: process.env.AUTH0_DOMAIN,
      clientId: process.env.AUTH0_CLIENT_ID,
      logoutRedirectUri: ""
    }
  },
  redirect: {
    login: "/",
    callback: "/callback",
    logout: "/"
  },
  plugins: ["@/plugins/helper.plugin", "@/plugins/api.plugin"]
};
