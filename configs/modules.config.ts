import { NuxtConfig } from "@nuxt/types";

export const modules: NuxtConfig["modules"] = [
  "@nuxtjs/axios",
  "@nuxtjs/auth-next",
  "@nuxtjs/dotenv",
  "@nuxtjs/toast",
  "nuxt-i18n"
];
