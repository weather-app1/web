# Weather App (WEB)

Web application for weather forecasts.

Built with:
- Nuxt.js
- Vuetify
- Typescript
- auth/Nuxt
- nuxt-i18n

## Installation
1. Download or clone the repository to your system.
2. Install the dependencies
```bash
npm install
```

## Run the project locally

1. Make a copy of the .env.example file and rename the copy to .env
2. Fill in all the empty variables in the .env file (will be provided).
3. Run project
```bash
npm run dev
```
