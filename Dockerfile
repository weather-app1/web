FROM node:12-alpine

WORKDIR /src/app

COPY package*.json ./
RUN npm install

COPY . .
RUN rm -f .env.production && mv .env.staging .env
RUN npm run build

RUN npm cache clean --force

EXPOSE 8080:8080
CMD ["npm", "start"]