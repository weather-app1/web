export default {
  pageTitle: {
    home: "Home"
  },
  label: {
    addCity: "Add City",
    condition: "Condition",
    coordinates: "Coordinates",
    degree: "Degree",
    high: "High",
    latitude: "Latitude",
    login: "Login",
    logout: "Log Out",
    longitude: "Longitude",
    low: "Low",
    noRecordFound: "No Record Found",
    notVerified: "Not Verified",
    searchYourCity: "Search your city...",
    signUp: "Sign Up",
    speed: "Speed",
    topCities: "Top Cities",
    wind: "Wind"
  },
  message: {
    addedToYourFavorites: "Added to your favorites.",
    checkEmailVerification:
      "Please check your email for the verification link.",
    plsFillInAllFields: "Please fill all the fields before submitting.",
    removedFromYourFavorites: "Removed from your favorites.",
    requestFailed: "Request failed. Please try again later."
  }
};
