export default {
  auth: {
    login: "auth/login",
    logout: "auth/logout",
    me: "profile"
  },
  city: {
    getAllOrCreate: "cities"
  },
  currentLocation: {
    get: "currentLocation"
  },
  forecast: {
    getAll: "forecast"
  },
  userLikes: {
    getOrCreateOrUpdateOrDelete: "userLikes"
  },
  weather: {
    get: "weather"
  }
};
