import BaseApi from "./BaseApi";
import { NuxtAxiosInstance } from "@nuxtjs/axios";

const version = BaseApi.VERSION;

export default class AuthApi extends BaseApi {
  constructor(axios: NuxtAxiosInstance) {
    super(axios);
    this.basePath = `api/${version}`;
  }

  async login() {
    const url = this.routes.auth.login;
    return await this.axios.$get(url);
  }

  async logout() {
    const url = `${this.basePath}/${this.routes.auth.logout}`;
    return await this.axios.$post(url);
  }

  async me() {
    const url = `${this.basePath}/${this.routes.auth.me}`;
    return await this.axios.$get(url);
  }
}
