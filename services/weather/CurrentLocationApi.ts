import BaseApi from "./BaseApi";
import { Helper } from "~/plugins/helper.plugin";
import { NuxtAxiosInstance } from "@nuxtjs/axios";

const version = BaseApi.VERSION;

export default class CurrentLocationApi extends BaseApi {
  constructor(axios: NuxtAxiosInstance) {
    super(axios);
    this.basePath = `api/${version}`;
  }

  async get() {
    const url = `${this.basePath}/${this.routes.currentLocation.get}`;
    return await this.axios.$get(url);
  }
}
