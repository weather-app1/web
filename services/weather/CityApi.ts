import BaseApi from "./BaseApi";
import { Helper } from "~/plugins/helper.plugin";
import { NuxtAxiosInstance } from "@nuxtjs/axios";

const version = BaseApi.VERSION;

export default class CityApi extends BaseApi {
  constructor(axios: NuxtAxiosInstance, private readonly helper: Helper) {
    super(axios);
    this.basePath = `api/${version}`;
  }

  async getAll(query: Object) {
    const url = `${this.basePath}/${
      this.routes.city.getAllOrCreate
    }?${this.helper.stringifyParams(query)}`;
    return await this.axios.$get(url);
  }

  async create(payload: Object) {
    const url = `${this.basePath}/${this.routes.city.getAllOrCreate}`;
    return await this.axios.$post(url, payload);
  }
}
