import BaseApi from "./BaseApi";
import { Helper } from "~/plugins/helper.plugin";
import { NuxtAxiosInstance } from "@nuxtjs/axios";

const version = BaseApi.VERSION;

export default class WeatherApi extends BaseApi {
  constructor(axios: NuxtAxiosInstance, private readonly helper: Helper) {
    super(axios);
    this.basePath = `api/${version}`;
  }

  async get(query: Object) {
    const url = `${this.basePath}/${
      this.routes.weather.get
    }?${this.helper.stringifyParams(query)}`;
    return await this.axios.$get(url);
  }
}
