import BaseApi from "./BaseApi";
import { Helper } from "~/plugins/helper.plugin";
import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { Auth } from "@nuxtjs/auth-next";

const version = BaseApi.VERSION;

export default class UserLikesApi extends BaseApi {
  constructor(
    axios: NuxtAxiosInstance,
    private readonly helper: Helper,
    private readonly auth: Auth
  ) {
    super(axios);
    this.basePath = `api/${version}`;
  }

  async getAll() {
    const url = `${this.basePath}/${this.routes.userLikes.getOrCreateOrUpdateOrDelete}/${this.auth.user?.sub}`;
    return await this.axios.$get(url);
  }

  async create() {
    const url = `${this.basePath}/${this.routes.userLikes.getOrCreateOrUpdateOrDelete}/${this.auth.user?.sub}`;
    return await this.axios.$post(url);
  }

  async update(payload: Object) {
    const url = `${this.basePath}/${this.routes.userLikes.getOrCreateOrUpdateOrDelete}/${this.auth.user?.sub}`;
    return await this.axios.$patch(url, payload);
  }

  async deleteAll() {
    const url = `${this.basePath}/${this.routes.userLikes.getOrCreateOrUpdateOrDelete}/${this.auth.user?.sub}`;
    return await this.axios.$delete(url);
  }
}
