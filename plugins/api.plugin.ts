import CitySunApi from "@/types/citysunApi";
import { Plugin } from "@nuxt/types";
import AuthApi from "~/services/weather/AuthApi";
import CityApi from "~/services/weather/CityApi";
import CurrentLocationApi from "~/services/weather/CurrentLocationApi";
import ForecastApi from "~/services/weather/ForecastApi";
import UserLikesApi from "~/services/weather/UserLikes";
import WeatherApi from "~/services/weather/WeatherApi";

interface Api {
  citysun: CitySunApi;
}

declare module "vue/types/vue" {
  interface Vue {
    $api: Api;
  }
}

declare module "@nuxt/types" {
  interface NuxtAppOptions {
    $api: Api;
  }
}

declare module "vuex/types/index" {
  interface Store<S> {
    $api: Api;
  }
}

const apiPlugin: Plugin = (context, inject) => {
  const { $axios, $helper, $auth } = context;

  const api: Api = {
    citysun: {
      auth: new AuthApi($axios),
      city: new CityApi($axios, $helper),
      currentLocation: new CurrentLocationApi($axios),
      forecast: new ForecastApi($axios, $helper),
      userLikes: new UserLikesApi($axios, $helper, $auth),
      weather: new WeatherApi($axios, $helper)
    }
  };

  inject("api", api);
};

export default apiPlugin;
